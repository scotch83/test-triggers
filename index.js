const express = require('express');
const server = express();
const axios = require('axios');

server.use(express.json());

server.post("/", (req, res) => {
    if (req.body.downloadUrl && req.query.url && req.body.automation.platform) {
        console.log('App flow payload', req.body);
        postDownloadUrlToAddress(req.body, req.body.automation.platform, req.body.downloadUrl, req.query.url)
            .then(result => res.sendStatus(result?.status || 200))
            .catch(err => {
                console.log("SERVER ERROR: " + err);
                res.sendStatus(err.reponse?.status || 500)
            })
    }
    else res.sendStatus(400).send("Missing parameters to perform request")
})

const SERVER_PORT = process.env.PORT || 3000;
server.listen(SERVER_PORT, () => {
    const os = require('os');
    const nInterfaces = os.networkInterfaces();
    console.log(`Server running at:`);
    for (const interfaceProp in nInterfaces) {
        if (nInterfaces.hasOwnProperty(interfaceProp)) {
            const element = nInterfaces[interfaceProp];
            element.filter(props => props.family === 'IPv4')
                .map(filteredInterface => {
                    console.log(`- ${filteredInterface.cidr.replace(/\/.*/, '')}:${SERVER_PORT}`)
                })
        }
    }
})


function postDownloadUrlToAddress(body, platform, downloadUrl, targetUrl) {
    console.log(`Triggering pipeline for app at ${downloadUrl} using url ${targetUrl} as a base`);
    if (targetUrl.includes('?'))
        targetUrl += `&`
    else targetUrl += `?`;

    const type = platform == 'android' ? 'apk' : 'ipa';
    const artifact = body.artifacts.find(a => a.artifactType.toLowerCase() === type);
    const app_name = artifact?.name || `${platform}-${body.type ?? ''}-${body.automation.environmentName ?? ''}-${body.commit?.sha?.substring(0, 8) ?? ''}.${type}`;

    const pipelineUrl = `${targetUrl}variables[DOWNLOAD_URL]=${encodeURIComponent(downloadUrl)}&variables[APP_NAME]=${encodeURIComponent(app_name)}&variables[ARTIFACT_TYPE]=${encodeURIComponent(type)}`;

    console.log(`Finally post to ${pipelineUrl}`);

    return axios.post(pipelineUrl);
}
